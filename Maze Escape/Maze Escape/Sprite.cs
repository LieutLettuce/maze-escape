﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Maze_Escape
{
    class Sprite
    {
        // --------------------
        // Data
        // --------------------
        protected Vector2 position;
        protected Texture2D texture;
        protected bool visable = true;


        // --------------------
        // Behaviour
        // --------------------
        public Sprite(Texture2D newTexture)
        {
            texture = newTexture;
        }
        // --------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (visable == true)
            {
                spriteBatch.Draw(texture, position, Color.White);
            }
        }
        // --------------------
        public bool GetVisable()
        {
            return visable;
        }
        // --------------------
        public void SetVisable(bool newVisable)
        {
            visable = newVisable;
        }
        // --------------------
    }
}
