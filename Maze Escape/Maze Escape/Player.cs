﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Maze_Escape
{
    class Player : Actor // Player Inherits from the ("is an ") Actor
    {

        // --------------------
        // Behaviour
        // --------------------
        public Player(Texture2D newTexture) : base(newTexture)
        {

        }
        // --------------------

        public override void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //Get teh current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // Check specific keys and record movement
            Vector2 movementInput = Vector2.Zero;

            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
            }

            if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
            }

            // add movement change to the velocity

            velocity += movementInput * 100000 * frameTime;

            // Apply drag from the ground
            velocity *= 0.4f; // TODO Remove Magic Number to BASE ONE

            //If the Speed is too high, clamp it to a reasonable max
            if (velocity.Length() > 500) // TODO Remove Magic Number to BASE ONE
            {
                velocity.Normalize();
                velocity *= 500; // TODO Remove Magic Number to BASE ONE
            }

            base.Update(gameTime);

        }
        // --------------------

    }
}
