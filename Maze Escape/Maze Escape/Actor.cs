﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Maze_Escape
{
    class Actor : Sprite
    {
        // --------------------
        // Data
        // --------------------
        protected Vector2 velocity = new Vector2(100,100);


        // --------------------
        // Behaviour
        // --------------------

        public Actor(Texture2D newTexture) : base(newTexture) // Passes the informationup to the parent (base) class (sprite)
        {
               
        }

        public virtual void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //Update position based on velocity and frame time]
            position += velocity * frameTime;
        }
    }
}
